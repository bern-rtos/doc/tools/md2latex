# md2LaTeX

This is a simple script that converts markdown files to LaTeX using pandoc. It also applies some project specific fixes.

## Prerequisites

You will need:
    - python 3
    - pandoc

Preferably create a virtual environment and install the dependencies:
```bash
pip install -r md2latex/requirements.txt
```

## Usage

### Config file

The scripts needs a config file that contain all the necessary paths and files.
An complete example is shown below:
```toml
[markdown]
directory = "../src"

[markdown.mdbook.summary]
file = "SUMMARY.md"
```

### Conversion command

```sh
(venv) python md2latex.toml
```

This will generate a combined LaTeX file in `generated/content.tex
