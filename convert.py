#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import os
import sys
import toml

ident = 4
work_dir = 'generated'
cmd_create_temp_dir = 'mkdir generated'
cmd_copy = 'cp -r {}/* generated/'
cmd_pandoc = 'pandoc -f markdown  -t latex --top-level-division=chapter {} -o generated/content.tex'

def parse_summary(file):
    files = []
    # skip everything that is not section
    for line in file:
        if (line[0] != '-') and (line[0] != ' '):
            continue
        
        # evaluate file level from preceding whitespaces
        level = re.search(r'^(\s*)', line).span()[1] // 4
        # get file
        file = re.search(r'\((.*?)\)', line).group(1)

        if len(file) > 0:
            files.append([level, file])

    return files


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('You must provide a config.toml file!')
        sys.exit(-1)
    config_file = sys.argv[1]
    config = toml.load(config_file)

    markdown_dir = config.get('markdown').get('directory')
    summary_file = os.path.join(markdown_dir, config.get('markdown').get('mdbook').get('summary').get('file'))

    
    with open(summary_file, 'r') as summary:
        files = parse_summary(summary)

    
    # pre-process markdown files
    os.system(cmd_create_temp_dir)
    os.system(cmd_copy.format(markdown_dir))
    for (level, file) in files:
        path = os.path.join(work_dir, file)
        with open(path, 'r') as md:
            content = md.read()

            # adjust headers over file structure
            if level == 1:
                content = re.sub(r'^([#]+)(?= \w)', r'\1#', content, flags=re.MULTILINE)
            elif level == 2:
                content = re.sub(r'^([#]+)(?= \w)', r'\1##', content, flags=re.MULTILINE)
        
        with open(path, 'w') as f:
            f.seek(0)
            f.truncate()
            f.write(content)

    # convert markdown to latex
    files_cmd = ' '.join([os.path.join(work_dir, item[1]) for item in files])
    os.system(cmd_pandoc.format(files_cmd))

    # post-process latex files
    converted = 'generated/content.tex'
    with open(converted, 'r') as latex:
        content = latex.read()

        # svg -> pdf
        content = re.sub(r'img/(.+)(.svg)', r'\1.pdf', content, flags=re.MULTILINE)
        # put images into figure environment
        content = re.sub(r'^(\\includegraphics{.*?})', r'\\begin{figure}[htb]\n\\centering\n\1\n\\end{figure}', content, flags=re.MULTILINE)
        
        # remove empty table heads
        content = re.sub(r'^([& ]+\\\\)\n\\midrule\n\\endhead', r'', content, flags=re.MULTILINE)
        # line for every row in tables
        content = re.sub(r'(\\\\)(?!(\n\\toprule)|(\n\\midrule)|(\n\\bottomrule))', r'\1\\midrule', content, flags=re.MULTILINE)

        # newpage for every requirement
        content = re.sub(r'^(\\hypertarget{(f|n)\d+-.*?}{)', r'\\newpage\n\1', content, flags=re.MULTILINE)
    
    with open(converted, 'w') as f:
        f.seek(0)
        f.truncate()
        f.write(content)